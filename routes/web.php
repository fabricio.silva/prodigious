<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PeopleController@index');
Route::get('people/edit/{id}', 'PeopleController@edit');
Route::post('people/save', 'PeopleController@save');
Route::get('people/create', 'PeopleController@create');
Route::post('people/delete', 'PeopleController@delete');
