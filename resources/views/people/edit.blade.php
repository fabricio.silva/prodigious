@extends('layouts.main')

@section('content')


<h1>{{ $person->name ? $person->name : 'Nova Pessoa' }}</h1>

<div class="row">
  <div class="col-md-8">
    <form action="{{ url('people/save') }}" method="post" enctype="multipart/form-data">
      {{ csrf_field() }}
      <input type="hidden" name="person_id" value="{{ $person->id }}">
    <div class="card-block">
      <div class="row">
        <div class="col-md-6">
          <div class="form-group">
            <label>Nome</label>
            <input type="text" class="form-control" name="name" required value="{{ $person->name ? $person->name : old('name')  }}">
          </div>
        </div>
        <div class="col-md-6">
          <div class="form-group">
            <label>Email</label>
            <input type="text" class="form-control" name="email" required value="{{ $person->email ? $person->email : old('email')  }}">
          </div>
        </div>
        <div class="col-md-12">
          <div class="form-group">
            <label>Descrição</label>
            <textarea rows="3" class="form-control" name="description" required>{{ $person->description ? $person->description : old('description')  }}</textarea>
          </div>
        </div>

        @if($person && $person->upload )
        <div class="col-md-3">
          <div class="form-group">

            <img src="{{$person->upload}}" width="auto" height="140">
          </div>
        </div>
        @endif
        <div class="col-md-6">
          <div class="form-group">
            <label>Foto</label>
            <input type="file" class="form-control" name="upload" value="{{ $person->upload ? $person->upload : old('upload')  }}">
          </div>
        </div>


      </div>
      <div class="row">

        <a href="{{ url('/') }}" class="btn btn-link" style="margin-left:15px;"><i class="fa fa-plus" aria-hidden="true"></i> ← Voltar para a Lista</a>
        @if($person->id)
        <a href="{{ url('people/create') }}" class="btn btn-link" style="margin-left:15px;"><i class="fa fa-plus" aria-hidden="true"></i> Nova Pessoa</a>
        @endif
        <button class="btn btn-primary" style="margin-left:15px;" type="submit"><i class="fa fa-check" aria-hidden="true"></i> Salvar</button>
      </form>


      @if($person->id)
        @include('layouts.delete', ['action' => 'people/delete', 'register' => $person->name, 'delete_id' => $person->id ])
      @endif

      </div>
    </div>

  </div>
</div>



@endsection
