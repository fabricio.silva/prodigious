@extends('layouts.main')
@section('content')
<div class="row">
  <div class="col col-md-12">
    <h1>Lista</h1>
  </div>
</div>

  <!-- START card -->
      <table class="table table-striped" id="tableWithSearch">
        <thead>
          <tr>
            <th></th>
            <th>Nome</th>
            <th>Email</th>
            <th></th>
          </tr>
        </thead>
        <tbody>
          @foreach ($people as $person)
          <tr>
            <td><img src="{{ $person->upload }}" width="auto" height="40"></td>
            <td>{{ $person->name }}</td>
            <td>{{ $person->email }}</td>
            <td>
              <a href="{{ url('people/edit/' . $person->id) }}"> Editar</a>
            </td>
          </tr>
          @endforeach

        </tbody>
      </table>


  <!-- END card -->
  <a href="{{ url('people/create') }}"> Nova Pessoa</a>
  @endsection
