
<button class="btn btn-danger" data-toggle="modal" type="button" data-target="#deleteModal{{ $delete_id }}" style="margin-left:15px;">
  <i class="fa fa-trash" aria-hidden="true"></i> Deletar
</button>
<div class="modal fade slide-up disable-scroll" id="deleteModal{{ $delete_id }}" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="false">
    <div class="modal-dialog ">
        <div class="modal-content-wrapper">

          <form action="{{ url($action) }}" method="post">
            <div class="modal-content">
                <div class="modal-header clearfix text-left">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                      <i class="pg-close fs-14"></i>
                    </button>
                    <h5>Você tem certeza?</h5>
                </div>
                <div class="modal-body">
                    {!! isset($info) ? $info : ''!!}
                    Ao confirmar, o registro <strong>{{ $register }}</strong> e todas suas dependencias serão removidos da base dados.
                </div>
                <div class="modal-footer">
                  {{ csrf_field() }}
                  <input type="hidden" name="delete_id" value="{{ $delete_id }}">
                  <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i> Cancelar</button>
                  <button type="submit" class="btn btn-danger"><i class="fa fa-trash" aria-hidden="true"></i> Deletar</button>
                </div>
            </div>
          </form>
        </div>
        <!-- /.modal-content -->
    </div>
</div>
