<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Storage;

use App\Person;

class PeopleController extends Controller
{

  public function index()
  {
    return view('people.index', ['people' => Person::all()] );
  }

  public function create()
  {
    $person = new Person();
    return view('people.edit', ['person' => $person]);
  }

  public function edit($id)
  {
    $person = Person::find($id);

    if(!$person)
    {
    flash('Pessoa informada não existe na base de dados', 'danger');
      return back();
    }
    return view('people.edit', ['person' => $person]);
  }


  public function save(Request $request)
  {


    $roles = [
      'name' => 'required|string|max:255',
      'email' => 'required|email',
      'description' => 'required|string',
      'upload' => 'image'
    ];

    $person = null;
    if($request->person_id)
    {
      $person = Person::find($request->person_id);
    }else{
      $person = new Person;
    }

    $validator = Validator::make($request->all(), $roles);

    if ($validator->fails()) {
        return redirect($request->person_id ? "people/edit/{$request->person_id}" : 'people/create')
                    ->withErrors($validator)
                    ->withInput();
        flash('Os dados informados inválidos. Revise e salve novamente.', 'danger');
    }
    $person->name = $request->name;
    $person->email = $request->email;
    $person->description = $request->description;

    if ($request->hasFile('upload'))
    {
      $path = Storage::putFile('public', $request->file('upload'));
      $url = Storage::url($path);
      $person->upload = $url;
    }
    $person->save();


    flash('Pessoa registrada com sucesso!', 'success');
    return redirect("/");
  }

  public function delete(Request $request)
  {
    $person = Person::find($request->delete_id);
    $person->delete();
    flash('A pessoa foi removida!', 'success');
    return redirect("/");
  }
}
