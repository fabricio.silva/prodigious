## Teste Prodigius
### Fabrício Silva
##### Após configuração do ambiente com php7+, laravel 5.8+ e mysql, executar os seguintes passos:
-- criar database no mysql

-- clonar o projeto: `git@gitlab.com:fabricio.silva/prodigious.git`

-- baixar as dependencias: `composer instal`

-- editar o arquivo `.env` com as credenciais da base

-- gerar a chave da app: `php artisan key:generation`

-- criar a tabela na base de dados: `php artisan migrate`

-- vincular o storage ao diretório público da aplicação: `php artisan storage:link`
